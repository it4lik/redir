#![allow(clippy::comparison_chain)]
use crate::{Map, Portal, Position, Redirection};

use itertools::Itertools;
use rayon::prelude::*;
use std::{cmp::Ordering, sync::mpsc};

pub fn bruteforce_grid(width: i16) {
    todo!()
    /* let mut longest_redirections: Vec<Redirection> = Vec::new();
    let mut current_max_size = 0;
    let mut i = 0;
    for positions in (0..=width).combinations_with_replacement(2).combinations(4) {
        let (p1, p2, p3, p4) = (&positions[0], &positions[1], &positions[2], &positions[3]);
        let portals: &[Portal] = &[
            Portal::new(Position::new(p1[0], p1[1])),
            Portal::new(Position::new(p2[0], p2[1])),
            Portal::new(Position::new(p3[0], p3[1])),
            Portal::new(Position::new(p4[0], p4[1])),
        ];

        let mut redirections: Vec<Redirection> = Vec::new();

        for index in 0..4 {
            i += 1;
            match Redirection::determine(index, portals) {
                Ok(redirection) => redirections.push(redirection),
                Err(error) => println!("ERROR: {:?}", error),
            }
        }
        redirections.sort_by_key(|r| r.size);
        if let Some(last_redirection) = redirections.pop() {
            match current_max_size.cmp(&last_redirection.size) {
                Ordering::Less | Ordering::Equal => {
                    println!(
                        "[{}] New maximum redirection {} -> {}",
                        i, current_max_size, last_redirection.size
                    );
                    current_max_size = last_redirection.size;
                    longest_redirections.append(
                        &mut redirections
                            .iter()
                            .filter(|r| r.size == current_max_size)
                            .cloned()
                            .collect_vec(),
                    );
                }
                Ordering::Greater => (),
            }
        }
    }
    println!("After {} iterations", i);
    for redirection in longest_redirections {
        println!("Redirection size: {}", current_max_size);
        Portal::debug(10, 10, &redirection.sorted_portals);
    } */
}

fn determine_redirection(positions: Vec<Vec<i8>>, tx: mpsc::Sender<Redirection>) {
    let (p1, p2, p3, p4) = (&positions[0], &positions[1], &positions[2], &positions[3]);
    let mut redirections: Vec<Redirection> = Vec::new();
    let portals: &[Portal] = &[
        Portal::new(Position::new(p1[0], p1[1])),
        Portal::new(Position::new(p2[0], p2[1])),
        Portal::new(Position::new(p3[0], p3[1])),
        Portal::new(Position::new(p4[0], p4[1])),
    ];
    for index in 0..4 {
        match Redirection::determine(index, portals) {
            Ok(redirection) => redirections.push(redirection),
            Err(error) => println!("ERROR: {:?}", error),
        }
    }
    redirections.sort_by_key(|r| r.size);

    let _ = tx.send(redirections[3].clone());
}

fn gather(redirections: Vec<Redirection>, tx: mpsc::Sender<Vec<Redirection>>) {
    let mut longest_redirections: Vec<Redirection> = Vec::new();
    let mut longest_size: usize = 0;
    for gathered_redirection in redirections {
        if gathered_redirection.size == longest_size {
            longest_redirections.push(gathered_redirection.clone());
        } else if gathered_redirection.size > longest_size {
            longest_size = gathered_redirection.size;
            longest_redirections.clear();
            longest_redirections.push(gathered_redirection.clone());
        }
    }
    let _ = tx.send(longest_redirections);
}

pub fn bruteforce_map(map: Map) {
    let usable_cells = map.usable_cells();
    // Convert usable_cells to match bruteforce_map arg type
    let mut usable_positions: Vec<Vec<i8>> = Vec::new();
    for pos in usable_cells.keys() {
        usable_positions.push(Vec::from([pos.x, pos.y]));
    }

    // Compute now (before any borrow) the total number of portals placement
    let redirections_count = usable_positions
        .clone()
        .into_iter()
        .combinations(4)
        .try_len()
        .unwrap();

    // Channel used to receive the best redirs for each portal placement
    let (tx, rx) = mpsc::channel();
    // Channel used to receive the best redir every 1000 redirs in r
    let (tx2, rx2) = mpsc::channel();

    println!("Computing redirs");
    // Find best redirs for each portals placement
    usable_positions
        .into_iter()
        .combinations(4)
        .par_bridge()
        .for_each(|positions| determine_redirection(positions, tx.clone()));

    {
        // Gathering results in r Vec
        println!("Searching longest redir among {}", redirections_count);
        let mut redirs: Vec<Redirection> = Vec::new();
        for _ in 0..redirections_count {
            redirs.push(rx.recv().unwrap());
        }

        redirs.chunks(1000).par_bridge().for_each(|redir| {
            gather(redir.to_vec(), tx2.clone());
        });
    }

    // Receive the longest redirs for every 1000 redirs in
    let mut redirs: Vec<Vec<Redirection>> = Vec::new();
    println!("{}", redirections_count);
    for _ in 0..(redirections_count / 1000) {
        redirs.push(rx2.recv().unwrap());
    }

    // Compute the longest redirs among all best_redir_among_1000
    let mut longest_size = 0;
    let mut longest: Vec<Redirection> = Vec::new();
    for r in redirs {
        for j in r {
            if j.size == longest_size {
                longest.push(j);
            } else if j.size > longest_size {
                longest_size = j.size;
                longest.clear();
                longest.push(j);
            }
        }
    }

    // Output
    for redirection in longest {
        println!("Redirection size: {}", redirection.size);
        println!("Portals: {:?}", redirection.sorted_portals);
        map.debug(&redirection.sorted_portals);
    }
}
