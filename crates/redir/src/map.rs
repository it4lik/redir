use crate::{Portal, Position};
use std::{collections::HashMap, fmt::Display};

pub const MAX_PORTAL: usize = 4;
pub const STANDARD_CELLS_COUNT: u16 = 560;
pub const STANDARD_WIDTH: u16 = 14;

#[derive(Debug, thiserror::Error)]
pub enum MapError {
    #[error("Portal cannot be place on {0}")]
    InvalidCell(String),
    #[error("Cell is out of bound")]
    OutOfBound,
}

#[derive(Debug, Default)]
pub struct Map {
    portals: Vec<Portal>,
    pub cells: HashMap<Position, Cell>,
}

impl Map {
    pub fn add_cell(&mut self, position: Position, cell: Cell) {
        self.cells.insert(position, cell);
    }

    pub fn place_portal(&mut self, position: Position) -> Result<(), MapError> {
        match self.cells.get(&position) {
            Some(cell) => match cell {
                Cell::Empty => {
                    if self.portals.len() == MAX_PORTAL {
                        self.portals.pop();
                    }
                    self.portals.insert(0, Portal::new(position));
                    Ok(())
                }
                Cell::Hole | Cell::Block => Err(MapError::InvalidCell(cell.to_string())),
            },
            None => Err(MapError::OutOfBound),
        }
    }

    pub fn debug(&self, portals: &[Portal]) {
        let offset = 20;
        for y in 0..33 {
            for x in 0..(offset + 13) {
                let symbol = match self.cells.get(&Position::new(x - offset, y)) {
                    Some(cell) => match cell {
                        Cell::Empty => {
                            let mut symbol = ".".to_string();
                            for (i, portal) in portals.iter().enumerate() {
                                if portal.position == Position::new(x - offset, y) {
                                    symbol = format!("{}", i.clone());
                                }
                            }
                            &symbol.to_owned()
                        }
                        Cell::Block => "#",
                        Cell::Hole => " ",
                    },
                    None => "*",
                };
                print!(" {} ", symbol);
            }
            println!();
        }
    }

    pub fn usable_cells(&self) -> HashMap<Position, Cell> {
        let mut usable_cells: HashMap<Position, Cell> = HashMap::new();
        let cells_iter = self.cells.clone().into_iter();

        let mut it = cells_iter.into_iter();
        loop {
            match it.next() {
                Some(c) => match c.1 {
                    Cell::Empty => {
                        usable_cells.insert(c.0, c.1);
                        ();
                    }
                    Cell::Hole => (),
                    Cell::Block => (),
                },
                None => break,
            }
        }
        usable_cells
    }
}

#[derive(Debug, Clone)]
pub enum Cell {
    Empty,
    Block,
    Hole,
}

impl Display for Cell {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Cell::Empty => write!(f, "Empty"),
            Cell::Block => write!(f, "Block"),
            Cell::Hole => write!(f, "Hole"),
        }
    }
}
