use clap::{Parser, Subcommand};
use dotenv::dotenv;

use redirzzz::{bf, Map, MapLoaderJson};

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// render an ASCII version of a given map
    Display {
        #[clap(long, short = 'd')]
        map_id: u32,
    },
    BruteforceGrid {
        #[clap(long, short = 'g')]
        width: i16,
    },
    BruteforceMap {
        #[clap(long, short = 'm')]
        map_id: u32,
    },
}

fn load_map_from_id(map_id: u32) -> Map {
    let data_dir = std::env::var("DATA_DIR").expect("DATA_DIR must be set in .env file");
    let json_maps_dir = format!("{data_dir}/json_maps");
    MapLoaderJson::from_file(&format!("{json_maps_dir}/{map_id}.json"))
}

fn main() {
    let cli = Cli::parse();
    dotenv().ok();

    match &cli.command {
        Commands::Display { map_id } => {
            let map = load_map_from_id(*map_id);
            map.debug(&[]);
        }
        Commands::BruteforceGrid { width } => {
            bf::bruteforce_grid(*width);
        }
        Commands::BruteforceMap { map_id } => {
            let map = load_map_from_id(*map_id);
            bf::bruteforce_map(map);
        }
    }
}
