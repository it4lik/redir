use crate::Position;

#[derive(Debug, Clone)]
pub struct Portal {
    pub position: Position,
}

impl Portal {
    pub fn new(position: Position) -> Portal {
        Portal { position }
    }

    // Debug function to print portals in a grid with their index.
    pub fn debug(width: i16, height: i16, portals: &[Portal]) {
        for y in 0..=height + 1 {
            if y == 0 || y == height + 1 {
                print!("#");
                for _ in 0..=width {
                    print!("###");
                }
                print!("#");
                println!();
            }
            if y < height + 1 {
                for x in 0..=width {
                    if x == 0 {
                        print!("#");
                    }
                    match portals
                        .iter()
                        .position(|r| r.position == Position::new(x as i8, y as i8))
                    {
                        Some(index) => print!("{:2} ", index),
                        None => print!(" . "),
                    }
                }
                print!("#");
            }
            println!();
        }
    }
}
