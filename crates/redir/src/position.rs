#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Position {
    pub x: i8,
    pub y: i8,
}

impl Position {
    pub fn new(x: i8, y: i8) -> Position {
        Position { x, y }
    }

    pub fn distance(&self, other: &Position) -> i8 {
        self.x.abs_diff(other.x) as i8 + self.y.abs_diff(other.y) as i8
    }

    pub fn add(&self, other: &Position) -> Position {
        Position::new(self.x + other.x, self.y + other.y)
    }
}
