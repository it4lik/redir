import sys
from typing import Optional
from portal import Portal
from position import Position as Pos
from directions import DIRECTIONS


class Redir:
    def __init__(self, portals: list, entryPortal: Portal):
        self.portals = portals
        self.sortedPortals = self.determineRedir(entryPortal)
        self.size = self.computeSize()

    def determineRedir(self, entryPortal) -> list[Portal]:
        currentPortal = entryPortal
        print(f"INFO : Entry portal is {currentPortal}")

        sortedPortals = []
        sortedPortals.append(currentPortal)

        leftPortals = list(self.portals)
        leftPortals.remove(currentPortal)

        for _ in range(0, len(leftPortals)):
            currentClosestPos = self.determineClosest(currentPortal, leftPortals)

            for portal in self.portals:
                if portal.pos == currentClosestPos:
                    sortedPortals.append(portal)
                    currentPortal = portal

            leftPortals.remove(currentPortal)

        return sortedPortals

    # return coordinates of the closest portal to givenPortal among portalsDict
    def determineClosest(self, givenPortal, portals) -> Optional[Pos]:
        # the closest recorded distance across the following for loop
        currentShortestDistance = 9999
        totalDistance = givenPortal.pos.distanceTo(portals[0].pos)

        # portals that are located at currentShortestDistance from givenPortal
        currentClosest = []
        # placeholder that will be returned
        closestPortal = None

        # iterate over all other portal coordinates in portalsDict
        for portal in portals:
            # don't try to compute distance between givenPortal and itself
            if portal == givenPortal:
                continue
            else:
                # compute distance between givenPortal and current portal
                totalDistance = givenPortal.pos.distanceTo(portal.pos)

                # if we found a portal closer than the others
                if totalDistance < currentShortestDistance:
                    currentShortestDistance = totalDistance
                    currentClosest.clear()
                    currentClosest.append(portal)
                # if we found a portal as close as one already remembered
                elif totalDistance == currentShortestDistance:
                    currentClosest.append(portal)

        # if we found one portal that is closer than the other
        if len(currentClosest) == 1:
            closestPortal = currentClosest[0].pos

        # equidistance with 2 portals
        elif len(currentClosest) == 2:
            print(f"INFO : Double equidistance from {givenPortal} to {currentClosest}")

            # equiIndexes represents a dict of every equidistant point
            # the key is the ordinal position going from the sacred point ("index")
            # the value is the coordinates of that point
            equiIndexes = self.getAllEquidistant(currentShortestDistance, givenPortal)

            # inside this dict, we locate the index of our 2 equidistant portals
            (indexP1, indexP2) = [
                equiIndexes.index(currentClosest[i].pos) for i in range(0, 2)
            ]

            circleHalfLength = 2 * currentShortestDistance
            # if there's a 180° angle, then the ordinal of the portals must be "opposite"
            ## in a equidistance at 4 cells, equiIndexes length is 16
            ## and thus, indexP1 and indexP2 MUST be separated by 8 indexes to have a 180°
            if abs(indexP1 - indexP2) == circleHalfLength:
                print("INFO : Double equi flat (180)")
                if 4 * currentShortestDistance - indexP1 < 8:
                    closestPortal = equiIndexes[indexP1]
                else:
                    closestPortal = equiIndexes[indexP2]
            else:
                print("INFO : Double equi contained in less than 180°")
                circleLength = 4 * currentShortestDistance

                # order indexP1 and indexP2 (P1 will always be smaller)
                if indexP1 > indexP2:
                    indexP1, indexP2 = indexP2, indexP1

                distP1P2 = indexP2 - indexP1
                if distP1P2 > circleHalfLength:
                    closestPortal = equiIndexes[indexP2]
                else:
                    closestPortal = equiIndexes[indexP1]

        # equidistance with 3 portals
        elif len(currentClosest) == 3:
            print(f"INFO : Triple equidistance from {givenPortal} to {currentClosest}")

            # quite the same logic as double equidistance
            equiIndexes = self.getAllEquidistant(currentShortestDistance, givenPortal)
            (indexP1, indexP2, indexP3) = [
                equiIndexes.index(currentClosest[i].pos) for i in range(0, 3)
            ]

            print(equiIndexes)

            # re-order variables so indexP1 is the smallest index
            # indexP3 the biggest, and indexP2 between
            if indexP1 > indexP2:
                indexP1, indexP2 = indexP2, indexP1
            if indexP1 > indexP3:
                indexP1, indexP3 = indexP3, indexP1
            if indexP2 > indexP3:
                indexP2, indexP3 = indexP3, indexP2

            circleLength = 4 * currentShortestDistance
            circleHalfLength = 2 * currentShortestDistance

            distP1P2 = min(
                abs(indexP2 - indexP1), circleLength - abs(indexP2 - indexP1)
            )
            distP2P3 = min(
                abs(indexP3 - indexP2), circleLength - abs(indexP3 - indexP2)
            )
            distP1P3 = min(
                abs(indexP3 - indexP1), circleLength - abs(indexP3 - indexP1)
            )

            print(
                f"P1:{indexP1} P2:{indexP2} P3:{indexP3} P1P2:{distP1P2} P1P3:{distP1P3} P2P3:{distP2P3}"
            )

            maxDist = max([distP1P2, distP1P3, distP2P3])

            contained = False
            containedOuter1 = None
            containedOuter2 = None

            if (maxDist == distP1P2) and (distP1P3 + distP2P3 == distP1P2):
                contained = True
                containedOuter1 = indexP1
                containedOuter2 = indexP2
            if (maxDist == distP2P3) and (distP1P3 + distP1P2 == distP2P3):
                contained = True
                containedOuter1 = indexP2
                containedOuter2 = indexP3
            if maxDist == distP1P3 and (distP1P2 + distP2P3 == distP1P3):
                contained = True
                containedOuter1 = indexP1
                containedOuter2 = indexP3

            if not contained:
                print(f"INFO : Triple equi NOT contained in 180° or less")
                # index 0 is the sacred point : most prio in this case
                for index in [indexP1, indexP2, indexP3]:
                    if index == 0:
                        closestPortal = equiIndexes[index]
                # otherwise, biggest index is always the closest to the sacred point (counter clockwise)
                else:
                    closestPortal = equiIndexes[indexP3]
            elif containedOuter1 is not None and containedOuter2 is not None:
                print(f"INFO : Triple equi contained in 180° or less")

                distOuter = abs(containedOuter2 - containedOuter1)
                if distOuter > circleHalfLength:
                    closestPortal = equiIndexes[containedOuter2]
                else:
                    closestPortal = equiIndexes[containedOuter1]
            else:
                print(
                    f"ERROR : Unexpected results containedOuter1:{containedOuter1} containedOuter2:{containedOuter2}"
                )

        if closestPortal is None:
            print("ERROR : No closest portal found")
        return closestPortal

    def getAllEquidistant(self, distance, givenPortal) -> list[Pos]:
        # le point sacré est toujours à la même abs que le portail d'entrée
        # et à la même ordonnée que le portail d'entrée + la distance d'équi
        sacredPoints = [
            givenPortal.pos + Pos(distance, 0),
            givenPortal.pos + Pos(0, distance),
            givenPortal.pos + Pos(-distance, 0),
            givenPortal.pos + Pos(0, -distance),
        ]

        equidistantPoints = []

        for i in range(0, len(DIRECTIONS)):
            for j in range(0, distance):
                point = sacredPoints[i] + Pos(
                    DIRECTIONS[i][0] * j, DIRECTIONS[i][1] * j
                )
                equidistantPoints.append(point)

        return equidistantPoints

    def computeSize(self) -> int:
        return (
            self.sortedPortals[0].pos.distanceTo(self.sortedPortals[1].pos)
            + self.sortedPortals[1].pos.distanceTo(self.sortedPortals[2].pos)
            + self.sortedPortals[2].pos.distanceTo(self.sortedPortals[3].pos)
        )
