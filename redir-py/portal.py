from position import Position as Pos


class Portal:
    def __init__(self, pos: Pos, index: int):
        self.pos = pos
        self.index = index

    def decrementIndex(self) -> None:
        self.index -= 1

    def __repr__(self) -> str:
        return f"Portal {self.pos}"
