class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distanceTo(self, otherCell: "Position") -> int:
        return abs(self.x - otherCell.x) + abs(self.y - otherCell.y)

    def __hash__(self) -> int:
        return hash((self.x, self.y))

    def __eq__(self, otherPos) -> bool:
        return self.x == otherPos.x and self.y == otherPos.y

    def __add__(self, otherPos) -> "Position":
        return Position(self.x + otherPos.x, self.y + otherPos.y)

    def __repr__(self) -> str:
        return f"P({self.x},{self.y})"
