from collections.abc import Iterator
from map import Map
from redir import Redir
from position import Position as Pos


def bf(_map: Map) -> None:
    print("Meo bonjour")

    biggest = 0
    biggestList = []

    for p1 in genPortalPos1():
        print(p1)
        _map.setPortalCell(Pos(p1[0], p1[1]))
        for p2 in genPortalPos2():
            _map.setPortalCell(Pos(p2[0], p2[1]))
            for p3 in genPortalPos3():
                _map.setPortalCell(Pos(p3[0], p3[1]))
                for p4 in genPortalPos4():
                    print(f"P1:{p1} P2:{p2} P3:{p3} P4:{p4}")
                    _map.setPortalCell(Pos(p4[0], p4[1]))

                    if (
                        (p1 == p2)
                        or (p1 == p3)
                        or (p1 == p4)
                        or (p2 == p3)
                        or (p2 == p4)
                        or (p3 == p4)
                    ):
                        _map.removeLastPortal()
                        continue

                    for i in range(0, 3):
                        print(_map.portals)
                        r = Redir(_map.portals, _map.portals[i])
                        print(_map.portals)
                        print(f"P1:{p1} P2:{p2} P3:{p3} P4:{p4} I:{i} S:{r.size}")
                        if r.size > biggest:
                            biggestList = []
                            biggestList.append(r.sortedPortals)
                            print(f"NEW BIGGEST : {r.size} is bigger than {biggest}")
                            biggest = r.size
                        elif r.size == biggest:
                            biggestList.append(r.sortedPortals)
                            print("EQUALS BIGGEST")
                    _map.removeLastPortal()
                _map.removeLastPortal()
            _map.removeLastPortal()
        _map.removeLastPortal()
    print(f"biggest size is {biggest} with portals {biggestList}")

    print()
    print(f"for example the first one is with portals {biggestList[0]} :")

    _map.portals = biggestList[0]
    _map.draw()


def genPortalPos1() -> Iterator[tuple[int, int]]:
    for i in range(0, 7):
        for j in range(0, 7):
            yield (i, j)


def genPortalPos2() -> Iterator[tuple[int, int]]:
    for i in range(0, 7):
        for j in range(0, 7):
            yield (i, j)


def genPortalPos3() -> Iterator[tuple[int, int]]:
    for i in range(0, 7):
        for j in range(0, 7):
            yield (i, j)


def genPortalPos4() -> Iterator[tuple[int, int]]:
    for i in range(0, 7):
        for j in range(0, 7):
            yield (i, j)
