# Redirectionzzzzz

## Usage

Create a `.env` according to `.env.sample` using an absolute path :

```bash
cp .env.sample .env
vim .env

cat .env
DATA_DIR=/var/lib/redir/data
```

You need to run the [map extractor](#map-extractor) first, before using any command.

```bash
# Build
cargo build

# Print usage
target/debug/portal-cli help

# ASCII rendering of a given map (using its map ID)
target/debug/portal-cli display --map-id 217059332

# bruteforce redir on given map (using its map ID)
target/debug/portal-cli bruteforce-map --map-id 217059332

# bruteforce redir on square map
target/debug/portal-cli bruteforce-grid --width 10
```

## Map Extractor

Simple `bash` script that extracts the maps from Dofus game files to a JSON format.  
Output will be saved in `utils/data/json_maps/`

```bash
bash utils/dofus_maps_as_json.sh
```
