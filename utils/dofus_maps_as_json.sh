#!/bin/bash

###
# Constants
###

# Colors
NC="\e[0m"
RE="\e[31m"
GR="\e[32m"
YE="\e[33m"
BO="\e[1m"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
ENV_FILE_PATH="${SCRIPT_DIR}/../.env"

DOFUS_PATH="/home/${USER}/.config/Ankama/Dofus"
DOFUS_MAPS_DIR="${DOFUS_PATH}/content/maps"

PYDOFUS_URL='https://github.com/balciseri/PyDofus'
PYDOFUS_PATH="$(pwd)/PyDofus"
dlm_unpack="${PYDOFUS_PATH}/dlm_unpack.py"

###
# Functions
###

log () {
  LEVEL="$1"
  MESSAGE="$2"

  if [[ "$LEVEL" == "INFO" ]]; then
    echo -e "${BO}[INFO]${NC} ${MESSAGE}"
  fi
  if [[ "$LEVEL" == "WARN" ]]; then
    echo -e "${BO}${YE}[WARN]${NC} ${MESSAGE}"
  fi
  if [[ "$LEVEL" == "ERROR" ]]; then
    echo -e "${BO}${RE}[ERROR]${NC} ${MESSAGE}"
  fi
}

###
# Checks
###
if [[ ! -f "${ENV_FILE_PATH}" ]]; then
  log ERROR ".env file does not exist at path ${ENV_FILE_PATH}."  
  exit 1
else
  source "${ENV_FILE_PATH}"
fi

if [[ ! -d "${DOFUS_PATH}" ]] ; then
  log ERROR "Dofus is not installed in the default location."
  exit 1
fi

if [[ ! -d "${DATA_DIR}" ]] ; then
  log WARN "Data directory not found, creating ${DATA_DIR}."
  mkdir "${DATA_DIR}"
fi

JSON_MAPS_DIRECTORY="${DATA_DIR}/json_maps"
if [[ ! -d "${JSON_MAPS_DIRECTORY}" ]] ; then
  log WARN "JSON maps directory not found, creating ${JSON_MAPS_DIRECTORY}."
  mkdir "${JSON_MAPS_DIRECTORY}"
fi

if [[ ! -d "${PYDOFUS_PATH}" ]] ; then
  log WARN "PyDofus not installed. Installing PyDofus in current directory : ${PYDOFUS_PATH}."
  git clone "${PYDOFUS_URL}" "${PYDOFUS_PATH}" &> /dev/null
fi

if [[ ! -d "${PYDOFUS_PATH}/input" ]] ; then
  mkdir "${PYDOFUS_PATH}/input"
fi

if [[ ! -d "${PYDOFUS_PATH}/output" ]] ; then
  mkdir "${PYDOFUS_PATH}/output"
fi

###
# Code
###

CURRENT_DIR="$(pwd)"
cd "${PYDOFUS_PATH}"

log INFO "Copying d2p files from game folder ${DOFUS_MAPS_DIR} to ${PYDOFUS_PATH}/input"
cp -R "${DOFUS_MAPS_DIR}"/*d2p "${PYDOFUS_PATH}/input"

log INFO "Extracting dlm maps from d2p files."
python "${PYDOFUS_PATH}/d2p_unpack.py" &> /dev/null

log INFO "Extracting json maps from dlm files, this may take a while."
find "${PYDOFUS_PATH}/output" -name "*.dlm" -exec python "${PYDOFUS_PATH}/dlm_unpack.py" {} \;

log INFO "Copying maps to data directory ${DATA_DIR}"
find "${PYDOFUS_PATH}/output" -name "*.json" -exec cp {} "${JSON_MAPS_DIRECTORY}" \;

log INFO "${BO}${GR}Success ! JSON maps are stored in ${DATA_DIR}.${NC}"

log INFO "You should remove artifacts with the following commands :
rm -rf ${PYDOFUS_PATH}/output/* ${PYDOFUS_PATH}/input/*"

cd "${CURRENT_DIR}"
